import 'package:flutter/material.dart';
import 'package:flutter_store/pages/product_detail.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  var product_list = [
    {
      "name": "SGK Lop 4",
      "image": "Images/Book/lop4.jpg",
      "price": 92,
      "old_price": 150,
    },
    {
      "name": "SGK Lop 5",
      "image": "Images/Book/lop5.jpg",
      "price": 100,
      "old_price": 120,
    },
    {
      "name": "Balo 1",
      "image": "Images/BackPack/backpack3.jpg",
      "price": 100,
      "old_price": 120,
    },
    {
      "name": "Balo 2",
      "image": "Images/BackPack/backpack4.jpg",
      "price": 100,
      "old_price": 120,
    },
    {
      "name": "Pen 1",
      "image": "Images/Pen/pen1.jpg",
      "price": 100,
      "old_price": 120,
    },
    {
      "name": "Pen 2",
      "image": "Images/Pen/pen2.jpg",
      "price": 100,
      "old_price": 120,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_list.length,
        gridDelegate:
        new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(4.0),
            child: Single_prod(
              prod_name: product_list[index]['name'],
              prod_image: product_list[index]['image'],
              prod_price: product_list[index]['price'],
              prod_old_price: product_list[index]['old_price'],
            ),
          );
        });
  }
}

class Single_prod extends StatelessWidget {
  final prod_name;
  final prod_image;
  final prod_price;
  final prod_old_price;

  Single_prod({
    this.prod_name,
    this.prod_image,
    this.prod_price,
    this.prod_old_price,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: prod_name,
        child: Material(
          child: InkWell(
            onTap: () => Navigator.of(context).push(new MaterialPageRoute(
              //truyền data từ product sang product-detail
                builder: (context) => new ProductDetails(
                    product_detail_name: prod_name,
                    product_detail_image: prod_image,
                    product_detail_price: prod_price,
                    product_detail_old_price: prod_old_price
                ))),
            child: GridTile(
                footer: Container(
                    color: Colors.white70,
                    child: Row(children: <Widget>[
                      Expanded(
                        child: Text(prod_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),),
                      ),

                      new Text("\$${prod_price}", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),)
                    ],)
                ),
                child: Image.asset(
                  prod_image,
                  fit: BoxFit.cover,
                )),
          ),
        ),
      ),
    );
  }
}
