import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          LoaiSP(
            image_location: 'Images/HorizonListIcon/backpack.png',
            image_caption: 'Cặp xách',
          ),
          LoaiSP(
            image_location: 'Images/HorizonListIcon/book.png',
            image_caption: 'Sách',
          ),
          LoaiSP(
            image_location: 'Images/HorizonListIcon/pen.png',
            image_caption: 'Bút viết',
          ),
          LoaiSP(
            image_location: 'Images/HorizonListIcon/discount.png',
            image_caption: 'Khuyến mãi',
          ),
        ],
      ),
    );
  }
}

class LoaiSP extends StatelessWidget {
  final String image_location;
  final String image_caption;

  LoaiSP({this.image_location,
    this.image_caption
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {},
        child: Container(
          width: 95.0,
          child: ListTile(
              title: Image.asset(
                image_location,
                width: 40.0,
                height: 40.0,
              ),
              subtitle: Container(
                alignment: Alignment.topCenter,
                child: Text(
                  image_caption,
                  style: new TextStyle(fontSize: 14.0),
                ),
              )),
        ),
      ),
    );
  }
}
