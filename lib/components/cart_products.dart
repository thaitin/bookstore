import 'package:flutter/material.dart';

class CartProducts extends StatefulWidget {
  @override
  _CartProductsState createState() => _CartProductsState();
}

class _CartProductsState extends State<CartProducts> {
  var Products_on_the_cart = [
    {
      "name": "SGK Lop 4",
      "image": "Images/Book/lop4.jpg",
      "price": 92,
      "quantity": 1
    },
    {
      "name": "SGK Lop 5",
      "image": "Images/Book/lop5.jpg",
      "price": 100,
      "quantity": 1
    }
  ];

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: Products_on_the_cart.length,
        itemBuilder: (context, index){
          return Single_cart_product(
            cart_prod_name: Products_on_the_cart[index]["name"],
            cart_prod_image: Products_on_the_cart[index]["image"],
            cart_prod_price: Products_on_the_cart[index]["price"],
            cart_prod_quantity: Products_on_the_cart[index]["quantity"],
          );
        });
  }
}

class Single_cart_product extends StatelessWidget {
  final cart_prod_name;
  final cart_prod_image;
  final cart_prod_price;
  final cart_prod_quantity;

  Single_cart_product({
    this.cart_prod_name,
    this.cart_prod_image,
    this.cart_prod_price,
    this.cart_prod_quantity
  });
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        // Hình sản phẩm
        leading: new Image.asset(cart_prod_image, width: 100.0,
          height: 100.0,),

        // Tên sản phẩm
        title: new Text(cart_prod_name),

        //Số lượng
        subtitle: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Text("Giá:"),
                ),

                Padding(padding: const EdgeInsets.fromLTRB(20.0, 8.0, 8.0, 8.0),
                  child: new Text("\$${cart_prod_price}",
                    style: TextStyle(
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.red
                    ),),
                )
              ],
            )
          ],
        ),
        trailing: FittedBox(
          fit: BoxFit.fill,
          child: Row(
            children: <Widget>[
              new IconButton(icon: Icon(Icons.arrow_left), onPressed: (){}),
              new Text("$cart_prod_quantity"),
              new IconButton(icon: Icon(Icons.arrow_right), onPressed: (){}),
            ],
          ),
        ),
      ),
    );
  }
}

