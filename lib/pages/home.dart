//import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
// Components
import 'package:flutter_store/components/horizontal_listview.dart';
import 'package:flutter_store/components/products.dart';
import 'package:flutter_store/pages/cart.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Widget image_carousel = new Container(
      height: 200.0,
      child: new Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('Images/BackPack/backpack.jpg'),
          AssetImage('Images/BackPack/backpack2.jpg'),
          AssetImage('Images/Book/book.jpg'),
          AssetImage('Images/Book/book2.jpg'),
        ],
        autoplay: true,
        dotSize: 4.0,
        indicatorBgPadding: 3.0,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        dotBgColor: Colors.transparent,
      ),
    );
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Colors.black,
        title: Text('ThaiTin-Shop'),
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          new IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new Cart()));
              })
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: Text('ThaiTin'),
              accountEmail: Text('thaitina5@gmail.com'),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.blueGrey,
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              decoration: new BoxDecoration(color: Colors.indigoAccent),
            ),
            //SideBar-Body
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Trang chủ'),
                leading: Icon(Icons.home, color: Colors.cyan),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Tài khoản'),
                leading: Icon(Icons.person, color: Colors.cyan),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Đơn hàng'),
                leading: Icon(Icons.shopping_basket, color: Colors.cyan),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new Cart()));
              },
              child: ListTile(
                title: Text('Giỏ hàng'),
                leading: Icon(Icons.shopping_cart, color: Colors.cyan),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Yêu thích'),
                leading: Icon(Icons.favorite, color: Colors.cyan),
              ),
            ),

            Divider(),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Cài đặt'),
                leading: Icon(Icons.settings, color: Colors.cyan),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Thông tin'),
                leading: Icon(Icons.help, color: Colors.cyan),
              ),
            )
          ],
        ),
      ),
      body: new Column(
        children: <Widget>[
          //Slide banner
          image_carousel,

          //Loại sản phẩm
          new Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  'Loại sản phẩm',
                  style: TextStyle(fontSize: 20),
                )),
          ),
          HorizontalList(),

          //List sản phẩm
          new Padding(
            padding: const EdgeInsets.all(22.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: new Text('Sản phẩm', style: TextStyle(fontSize: 20))),
          ),
          Flexible(
            child: Products(),
          )
        ],
      ),
    );
  }
}