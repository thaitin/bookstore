import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_store/pages/home.dart';
//import 'package:flutter_store/main.dart';

class ProductDetails extends StatefulWidget {
  final product_detail_name;
  final product_detail_image;
  final product_detail_price;
  final product_detail_old_price;

  ProductDetails(
      {this.product_detail_name,
        this.product_detail_image,
        this.product_detail_price,
        this.product_detail_old_price});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Colors.indigoAccent,
        title: InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => new HomePage()));
            },
            child: Text("ThaiTin Shop")),
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            height: 300.0,
            child: GridTile(
              child: Container(
                color: Colors.white,
                child: Image.asset(widget.product_detail_image),
              ),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  leading: new Text(
                    widget.product_detail_name,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                  title: new Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text(
                          "\$${widget.product_detail_old_price}",
                          style: TextStyle(
                              color: Colors.grey,
                              decoration: TextDecoration.lineThrough),
                        ),
                      ),
                      Expanded(
                          child: new Text(
                            "\$${widget.product_detail_price}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, color: Colors.red),
                          ))
                    ],
                  ),
                ),
              ),),
          ),

          //==============Hàng nút đầu tiên============

          Row(
            children: <Widget>[
              //========== Nút chọn size===========
              Expanded(
                child: MaterialButton(
                  onPressed: () {
                    showDialog(context: context,
                        builder: (context){
                          return new AlertDialog(
                            title: new Text("Size"),
                            content: new Text("Chọn size"),
                            actions: <Widget>[
                              new MaterialButton(onPressed: (){
                                Navigator.of(context).pop(context);
                              },
                                child: new Text("Đóng"),)
                            ],
                          );
                        });
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: new Text("Size")
                      ),
                      Expanded(
                          child: new Icon(Icons.arrow_drop_down)
                      ),
                    ],
                  ),),
              ),
              //========== Nút chọn Color===========
              Expanded(
                child: MaterialButton(
                  onPressed: () {
                    showDialog(context: context,
                        builder: (context){
                          return new AlertDialog(
                            title: new Text("Màu"),
                            content: new Text("Chọn màu"),
                            actions: <Widget>[
                              new MaterialButton(onPressed: (){
                                Navigator.of(context).pop(context);
                              },
                                child: new Text("Đóng"),)
                            ],
                          );
                        });
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: new Text("Màu")
                      ),
                      Expanded(
                          child: new Icon(Icons.arrow_drop_down)
                      ),
                    ],
                  ),),
              ),
              //========== Nút chọn Quantity===========
              Expanded(
                child: MaterialButton(
                  onPressed: () {
                    showDialog(context: context,
                        builder: (context){
                          return new AlertDialog(
                            title: new Text("Số lượng"),
                            content: new Text("Chọn s.lượng"),
                            actions: <Widget>[
                              new MaterialButton(onPressed: (){
                                Navigator.of(context).pop(context);
                              },
                                child: new Text("Đóng"),)
                            ],
                          );
                        });
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: new Text("S.lượng")
                      ),
                      Expanded(
                          child: new Icon(Icons.arrow_drop_down)
                      ),
                    ],
                  ),),
              )
            ],
          ),

          //==============Hàng nút thứ hai============

          Row(
            children: <Widget>[
              //========== Nút chọn size===========
              Expanded(
                child: MaterialButton(onPressed: (){},
                    color: Colors.deepPurpleAccent,
                    textColor: Colors.white,
                    elevation: 0.2,
                    child: new Text("Mua ngay")
                ),
              ),
              new IconButton(icon: Icon(Icons.add_shopping_cart, color: Colors.deepPurpleAccent,), onPressed: (){}),
              new IconButton(icon: Icon(Icons.favorite_border, color: Colors.deepPurpleAccent,), onPressed: (){})
            ],
          ),

          Divider(),

          new ListTile(
            title: new Text("Chi tiết sản phẩm"),
            subtitle: new Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"),
          ),

          Divider(),

          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12.0, 5.0 , 5.0, 5.0),
                child: new Text("Tên sản phẩm", style: TextStyle(color: Colors.grey),),),
              Padding(padding: EdgeInsets.all(5.0),
                child: new Text(widget.product_detail_name),)
            ],
          ),

          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12.0, 5.0 , 5.0, 5.0),
                child: new Text("Thương hiệu", style: TextStyle(color: Colors.grey),),),

              Padding(padding: EdgeInsets.all(5.0),
                child: new Text("........"),)
            ],
          ),

          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12.0, 5.0 , 5.0, 5.0),
                child: new Text("Tình trạng", style: TextStyle(color: Colors.grey),),),
              Padding(padding: EdgeInsets.all(5.0),
                child: new Text("........."),)
            ],
          )
        ],
      ),
    );
  }
}

