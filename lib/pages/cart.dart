import 'package:flutter/material.dart';
import 'package:flutter_store/components/cart_products.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Colors.indigoAccent,
        title: Text('Giỏ hàng'),
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),

      body: new CartProducts(),

      bottomNavigationBar: new Container(
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Expanded(child: ListTile(
              title: new Text("Tổng tiền:"),
              subtitle: new Text("\$2230"),
            )),

            Expanded(
              child: new MaterialButton(onPressed: (){},
                  child: new Text("Thanh toán", style: TextStyle(color: Colors.white),),
                  color: Colors.deepPurpleAccent),
            )
          ],
        ),
      ),
    );
  }
}
